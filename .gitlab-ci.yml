# Slice CI/CD pipeline configuration file

image: python:3.9.10-slim-buster

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  PYLINT_FAIL_UNDER: 8

before_script:
  - python -V
  - python -m pip install virtualenv
  - python -m venv $CI_PROJECT_DIR/slice/venv
  - source $CI_PROJECT_DIR/slice/venv/bin/activate
  - python -m pip install pip wheel setuptools build twine pylint pytest --upgrade
  - python -m pip install -r requirements.txt --upgrade

stages:
  - test
  - build
  - checking
  - deploy

pylint:           # This job runs Python linter (PyLint)
  stage: test
  script:
    - echo "Running code linting"
    - python -m pylint --fail-under=$PYLINT_FAIL_UNDER $CI_PROJECT_DIR/slice/*.py

pytest:           # This job runs all unitary tests (PyTest)
  stage: test
  rules:
    - if: '$CI_COMMIT_TAG'
  script:
    - echo "Running unitary tests"
    - python -m pytest

app-build:
  stage: build
  needs:
    - pylint
    - pytest
  rules:
    - if: '$CI_COMMIT_TAG'
  script:
    - echo "Building application"
    - python -m build
  artifacts:
    untracked: false
    expire_in: 7 days
    paths:
      - dist
  
pypi-check-deploy:  # This job checks package info before deploying to PyPi repository
  stage: checking
  needs:
    - app-build
  rules:
    - if: '$CI_COMMIT_TAG'
  script:
    - echo "Checking PyPi deployment"
    - python -m twine check dist/*
  artifacts:
    untracked: false
    expire_in: 7 days
    paths:
      - dist

pypi-deploy:        # This job deploys package to PyPi repository
  stage: deploy
  needs:
    - pypi-check-deploy
  rules:
    - if: '$CI_COMMIT_TAG'
  script:
    - echo "Deploying application, please hold your breath..."
    - python -m twine upload dist/* --verbose -u "__token__" -p "$PYPI_API_TOKEN"