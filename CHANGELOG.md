# Slice v1.0.2 (2022-03-07)

## Minor package cleaning

### Bug fixes

+ None

### Additions

+ None

### Deprecations

+ None

### Deletions

+ None

### Miscellaneous

+ Using dynamic semver for CLI tool
+ README.md typo fix

---

# Slice v1.0.1 (2022-03-07)

## Minor package cleaning

### Bug fixes

+ None

### Additions

+ None

### Deprecations

+ None

### Deletions

+ None

### Miscellaneous

+ None

---

# Slice v1.0.0 (2022-03-06)

## First program release :)

### Bug fixes

+ None

### Additions

+ By sized chunk slicing feature
+ By wanted parts count slicing feature

### Deprecations

+ None

### Deletions

+ None

### Miscellaneous

+ None
