"""Slice module

Import slicing features
"""

__version__ = '1.0.2'

from .slicing import (
    slice_by_size,
    slice_by_count
)
